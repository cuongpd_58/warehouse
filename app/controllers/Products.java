package controllers;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import models.Product;
import models.StockItem;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.products.details;
import views.html.products.list;
import java.util.ArrayList;
import java.util.List;

public class Products extends Controller
{
	private static final Form<Product> productForm = Form.form(Product.class);
	public static Result list()
	{
		List<Product> products = Product.findAll();
		return ok(list.render(products));
	}
	
	public static Result newProduct()
	{
		return ok(details.render(productForm));
	}
	
	public static Result details(Product product)
	{
		if (product == null) {
			return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	
	public static Result save()
	{
		Form<Product> boundForm = productForm.bindFromRequest();
		Product product = boundForm.get();
		StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
       
        product.save();
        stockItem.save();
		
		
		if (boundForm.hasErrors()) 
		{
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		}
		
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
			if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			}	
		}
		product.tags = tags;
		if (product.id == null) {
            product.save();
        } else {
            product.update();
        }
		Ebean.save(product);
		flash("success", String.format("Successfully added product %s", product));
		
		return redirect(routes.Products.list());
	}
	
	public static Result delete(String ean) 
	{
		final Product product = Product.findByEan(ean);
		if(product == null) 
		{
			return notFound(String.format("Product %s does not exists.", product.ean));
		}
		for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
		product.delete();
		return redirect(routes.Products.list());
	}
}