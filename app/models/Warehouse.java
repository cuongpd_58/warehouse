package models;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
 @Entity
public class Warehouse {
	@Id
	public Long id;
	
    public String name;
	@OneToOne
	public Address address;
    public List<StockItem> stock = new ArrayList(); 
 
    public String toString() {
        return name;
    }
}