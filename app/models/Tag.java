package models;
 
import play.data.validation.Constraints;
import java.util.*;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import play.db.ebean.Model;
import javax.persistence.*;


@Entity
public class Tag{
    public Long id;
    public String name;
	@ManyToMany(mappedBy="tags")
    public List<Product> products;
	public Tag(){
        // Left empty
    }
    public Tag(Long id, String name, Collection<Product> products) {
        this.id = id;
        this.name = name;
        this.products = new LinkedList<Product>(products);
        for (Product product : products) {
            product.tags.add(this);
        }
    }

    public static Tag findById(Long id) {
        return Tag.find.byId(id);
    }
}